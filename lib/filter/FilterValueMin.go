package filter

import (
	"bitbucket.org/omie/homes/models/property"
	log "github.com/Sirupsen/logrus"
)

// type that represents this filter
type FilterValueMin struct {
	MinValue int
}

// filter out a record if its value is less than given minimum value
func (self *FilterValueMin) Filter(p *property.Property) bool {
	if p.Value < self.MinValue {
		log.Debug("--- MinValue:", p.Value, " is less than ", self.MinValue)
	}
	return p.Value < self.MinValue
}
