package filter

import (
	"regexp"

	"bitbucket.org/omie/homes/models/property"
	log "github.com/Sirupsen/logrus"
)

// type that represents this filter
type FilterStreetAddressIgnoreString struct {
	FilterString string
}

// filter out a record if it contains given string
func (self *FilterStreetAddressIgnoreString) Filter(p *property.Property) bool {

	// `\\b` is used to check based on word boundary
	match, err := regexp.MatchString("\\b"+self.FilterString+"\\b", p.StreetAddress)
	if err != nil {
		log.Error("FilterStreetAddressIgnoreString:", err.Error())
		return false
	}

	if match {
		log.Debug("--- Street:", p.StreetAddress, " contains ", self.FilterString)
	}
	return match
}
