package filter

import (
	"bitbucket.org/omie/homes/models/property"
)

// Filter interface - should be implemented by every filter
type Filter interface {
	Filter(p *property.Property) bool
}
