package filter

import (
	"sync"

	"bitbucket.org/omie/homes/models/property"
	log "github.com/Sirupsen/logrus"
)

type FilterSkipNth struct {
	N     int
	count int
	sync.Mutex
}

func (self *FilterSkipNth) Filter(p *property.Property) bool {
	self.Lock()
	defer self.Unlock()

	self.count++

	if self.count%self.N == 0 {
		log.Debug("--- SkipNth.Filter: count:", self.count, " skip? ", self.count%self.N == 0)
	}
	return self.count%self.N == 0
}
