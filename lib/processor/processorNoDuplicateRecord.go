package processor

import (
	"sync"

	"bitbucket.org/omie/homes/lib/cache"

	log "github.com/Sirupsen/logrus"
)

// represents a processor
type ProcessorNoDuplicateRecord struct {
	BaseProcessor
}

// interface method
// listens onto a channel for a record or close signal
// process the record and push it to the store if it
// passes all the rules
func (self *ProcessorNoDuplicateRecord) Process(outChan <-chan []string, wg *sync.WaitGroup) {
	defer wg.Done()

	processedRecordsCount := 0
	failCount := 0
	markedForDeletion := make([]string, 0)

	for {
		record, more := <-outChan
		if !more {
			break
		}
		processedRecordsCount++

		p, err := self.baseProcess(record)
		if err != nil {
			log.Warn(err.Error())
			failCount++
			continue
		}

		// in case of duplicate records we are supposed to delete all the instances of such record
		// check if it exists and mark it for deletion later
		key := p.GetKey()

		if cache.Exists(key) {
			markedForDeletion = append(markedForDeletion, key)
			failCount++
			continue
		}

		// If we reach here then it means this record is going to make it the dedup'ed set,
		// lets run available filters on it first
		result := false
		for _, f := range self.filters {
			result = result || f.Filter(p)
		}
		if result {
			// if any of the filter reported true, skip this record
			failCount++
			continue
		}

		cache.Set(key, *p)
	}
	// now delete the records which were marked for deletion
	for _, k := range markedForDeletion {
		cache.Clear(k)
	}

	log.Info("Processed records:", processedRecordsCount)
	log.Info("Failures:", failCount)
}
