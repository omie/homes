package processor

import (
	//"fmt"
	"sort"
	"sync"
	"time"

	"bitbucket.org/omie/homes/lib/cache"
	"bitbucket.org/omie/homes/lib/scanner"
	//"bitbucket.org/omie/homes/models/property"

	log "github.com/Sirupsen/logrus"
)

// initialize scanner and spawn given number of ProcessorLastRecord
// wait for all the processors to return and print the list
func DoWork(filename string, processor Processor, numberOfWorkers int) {
	start := time.Now()

	outChan, err := scanner.ScanFile(filename)
	if err != nil {
		log.Error("err:", err.Error())
		return
	}

	// should flush the store since other processors may have used it before
	cache.Flush()

	wg := new(sync.WaitGroup)
	for i := 0; i < numberOfWorkers; i++ {
		wg.Add(1)
		go processor.Process(outChan, wg)
	}
	wg.Wait()

	elapsed := time.Since(start)
	log.Printf("completed in %s", elapsed)

	printList()
}

// print current state of our store
func printList() {
	keys := cache.Keys()
	sort.Strings(keys)
	log.Println("Total records to print: ", len(keys))

	/*
		var p property.Property
		for _, k := range keys {
			p = cache.Get(k)
			fmt.Println(p.String())
		}
	*/
}
