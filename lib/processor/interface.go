package processor

import (
	"bitbucket.org/omie/homes/lib/filter"
	"sync"
)

// Processor Interface - should be implemented by every processor
type Processor interface {
	Process(outChan <-chan []string, wg *sync.WaitGroup)
	SetFilters(args ...filter.Filter)
}
