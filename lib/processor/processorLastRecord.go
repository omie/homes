package processor

import (
	"sync"

	"bitbucket.org/omie/homes/lib/cache"

	log "github.com/Sirupsen/logrus"
)

// represents a processor
type ProcessorLastRecord struct {
	BaseProcessor
}

// interface method
// listens onto a channel for a record or close signal
// process the record and push it to the store if it
// passes all the rules
func (self *ProcessorLastRecord) Process(outChan <-chan []string, wg *sync.WaitGroup) {
	defer wg.Done()

	processedRecordsCount := 0
	failCount := 0

	for {
		record, more := <-outChan
		if !more {
			break
		}

		processedRecordsCount++

		p, err := self.baseProcess(record)
		if err != nil {
			log.Warn(err.Error())
			failCount++
			continue
		}

		// If we reach here then it means this record is going to make it the dedup'ed set,
		// lets run available filters on it first
		result := false
		for _, f := range self.filters {
			result = result || f.Filter(p)
		}
		if result {
			// if any of the filter reported true, skip this record
			failCount++
			continue
		}

		// we are supposed to use the last record in case of duplicates
		// this will overwrite previous record if it exists
		cache.Set(p.GetKey(), *p)
	}
	log.Info("Processed records:", processedRecordsCount)
	log.Info("Failures:", failCount)
}
