package processor

import (
	"bitbucket.org/omie/homes/lib/filter"
	"bitbucket.org/omie/homes/models/property"
	"errors"
)

// base type that other filters can use to instantly get SetFilters() implementation
type BaseProcessor struct {
	filters []filter.Filter
}

// processor interface method
// sets given list of filters internally for use
func (b *BaseProcessor) SetFilters(args ...filter.Filter) {
	for _, f := range args {
		b.filters = append(b.filters, f)
	}
}

// helper functions abstracting common tasks in Process()
// derived processors must implement their own Process()
// hence this is named baseProcess() and is not exported.
// This does not abstract out filters run because that is upto
// the processor when to run filters, before or after dedup
func (b *BaseProcessor) baseProcess(record []string) (*property.Property, error) {
	if len(record) != 5 {
		return nil, errors.New("Invalid record")
	}

	p, err := property.NewFromRow(record[0], record[1], record[2], record[3], record[4])
	if err != nil {
		return nil, err
	}

	return p, nil
}
