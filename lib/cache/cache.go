package cache

import (
	"sync"

	"bitbucket.org/omie/homes/models/property"
)

var store map[string]property.Property
var lock sync.RWMutex

// initialize local store
func init() {
	store = make(map[string]property.Property)
}

// Get an object from the store if it exists,
// returns nil otherwise
func Get(key string) property.Property {
	lock.RLock()
	defer lock.RUnlock()

	return store[key]
}

// Set an object in the store
func Set(key string, val property.Property) {
	lock.Lock()
	defer lock.Unlock()

	store[key] = val
}

// Delete an object from the store
func Clear(key string) {
	lock.Lock()
	defer lock.Unlock()

	delete(store, key)
}

// Check if object exists in the store for given key
func Exists(key string) bool {
	lock.RLock()
	defer lock.RUnlock()

	_, ok := store[key]
	return ok
}

// Return a list of all keys
func Keys() []string {
	lock.RLock()
	defer lock.RUnlock()

	keys := make([]string, 0, len(store))
	for k := range store {
		keys = append(keys, k)
	}
	return keys
}

// Delete everything from the store
func Flush() {
	lock.Lock()
	defer lock.Unlock()

	store = make(map[string]property.Property)
}
