package scanner

import (
	"encoding/csv"
	"errors"
	"os"
)

// start scanning line by line off a given Reader
// push scanned record on outChan
// push a signal on eofChan upon EOF
func scan(reader *os.File, outChan chan<- []string) {
	// this function requires reader to be os.File specifically.
	// We could use io.Reader type to make it more flexible but then we would
	// require additional engineering to be able to close opened file descriptor
	defer reader.Close()

	r := csv.NewReader(reader)
	for {
		record, err := r.Read()

		if err != nil {
			close(outChan)
			break
		}
		outChan <- record
	}
}

// scans a given file line-by-line and returns it on channel
// returns receive-only channel
func ScanFile(filename string) (<-chan []string, error) {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return nil, errors.New("could not find the file specified: " + filename)
	}

	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}

	outChan := make(chan []string)
	go scan(file, outChan)

	return outChan, nil
}
