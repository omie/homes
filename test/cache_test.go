package test

import (
	"testing"

	"bitbucket.org/omie/homes/lib/cache"
	"bitbucket.org/omie/homes/models/property"
)

const testKey string = "1-1/01/15"

var testVal, _ = property.NewFromRow("1", "1 Northburn RD", "WANAKA", "1/01/15", "280000")

// Test that object can be set in the cache
func TestGetSet(t *testing.T) {
	cache.Set(testKey, *testVal)

	fromCache := cache.Get(testKey)

	if fromCache.Id != testVal.Id {
		t.Errorf("Unexpected value returned from cache")
	}

}

// Test that object can be removed from cache
func TestClear(t *testing.T) {
	cache.Set(testKey, *testVal)
	cache.Clear(testKey)

	exists := cache.Exists(testKey)

	if exists {
		t.Errorf("Unexpected value to be cleared from cache")
	}

}

// Test that slice of keys is returned from cache
func TestKeys(t *testing.T) {
	cache.Set(testKey, *testVal)
	keys := cache.Keys()

	if len(keys) != 1 {
		t.Errorf("Unexpected number of keys returned from cache")
	}

}

// Test that cache can be flushed
func TestFlush(t *testing.T) {
	cache.Flush()

	keys := cache.Keys()

	if len(keys) != 0 {
		t.Errorf("Cache failed to flush")
	}

}
