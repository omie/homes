package test

import (
	"testing"

	// log "github.com/Sirupsen/logrus"

	"bitbucket.org/omie/homes/models/property"
)

// Test that property object is created successfully
func TestNewFromRow(t *testing.T) {
	// 1,1 Northburn RD,WANAKA,1/01/15,280000
	p, err := property.NewFromRow("1", "1 Northburn RD", "WANAKA", "1/01/15", "280000")

	if err != nil {
		t.Errorf("Unexpected error: %s", err.Error())
	}

	if p.Id != 1 {
		t.Errorf("Unexpected Id: %d", p.Id)
	}

	if p.StreetAddress != "1 Northburn RD" {
		t.Errorf("Unexpected StreetAddress: %s", p.StreetAddress)
	}

	if p.Town != "WANAKA" {
		t.Errorf("Unexpected Town: %s", p.Town)
	}

	if p.ValuationDate.Format("01/02/06") != "01/01/15" {
		t.Errorf("Unexpected ValuationDate: %s", p.ValuationDate)
	}

	if p.Value != 280000 {
		t.Errorf("Unexpected Value: %d", p.Value)
	}
}

// Test ID validation
func TestNewFromRow_WrongId(t *testing.T) {
	p, err := property.NewFromRow("qwerty", "1 Northburn RD", "WANAKA", "1/01/15", "280000")

	if err == nil {
		t.Errorf("Expected error but found nil")
	}

	if p != nil {
		t.Errorf("Expected property object to be nil")
	}
}

// Test ValuationDate validation
func TestNewFromRow_WrongValuationDate(t *testing.T) {
	p, err := property.NewFromRow("1", "1 Northburn RD", "WANAKA", "51/01/15", "280000")

	if err == nil {
		t.Errorf("Expected error but found nil")
	}

	if p != nil {
		t.Errorf("Expected property object to be nil")
	}
}

// Test Value validation
func TestNewFromRow_WrongValue(t *testing.T) {
	p, err := property.NewFromRow("1", "1 Northburn RD", "WANAKA", "1/01/15", "abcdef")

	if err == nil {
		t.Errorf("Expected error but found nil")
	}

	if p != nil {
		t.Errorf("Expected property object to be nil")
	}
}

// Test StreetAddress validation
func TestNewFromRow_WrongStreetAddress(t *testing.T) {
	p, err := property.NewFromRow("1", "      ", "WANAKA", "1/01/15", "280000")

	if err == nil {
		t.Errorf("Expected error but found nil")
	}

	if p != nil {
		t.Errorf("Expected property object to be nil")
	}
}

// Test Town validation
func TestNewFromRow_WrongTown(t *testing.T) {
	p, err := property.NewFromRow("1", "1 Northburn RD", "     ", "1/01/15", "280000")

	if err == nil {
		t.Errorf("Expected error but found nil")
	}

	if p != nil {
		t.Errorf("Expected property object to be nil")
	}
}
