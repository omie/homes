package test

import (
	"testing"

	"bitbucket.org/omie/homes/lib/filter"
	"bitbucket.org/omie/homes/models/property"
)

var testProperty, _ = property.NewFromRow("1", "1 Northburn RD", "WANAKA", "1/01/15", "280000")

// Test FilterValueMin successful
func TestFilterValueMin_successful(t *testing.T) {
	f := filter.FilterValueMin{MinValue: 500000}
	result := f.Filter(testProperty)

	if !result {
		t.Errorf("Expected true from FilterValueMin but received false")
	}

}

// Test FilterValueMin unsuccessful
func TestFilterValueMin_unsuccessful(t *testing.T) {
	f := filter.FilterValueMin{MinValue: 5000}
	result := f.Filter(testProperty)

	if result {
		t.Errorf("Expected false from FilterValueMin but received true")
	}

}

// Test FilterStreetAddressIgnoreString successful
func TestFilterStreetAddressIgnoreString_successful(t *testing.T) {
	f := filter.FilterStreetAddressIgnoreString{FilterString: "RD"}
	result := f.Filter(testProperty)

	if !result {
		t.Errorf("Expected true from FilterStreetAddressIgnoreString but received false")
	}

}

// Test FilterStreetAddressIgnoreString unsuccessful
func TestFilterStreetAddressIgnoreString_unsuccessful(t *testing.T) {
	f := filter.FilterStreetAddressIgnoreString{FilterString: "AVL"}
	result := f.Filter(testProperty)

	if result {
		t.Errorf("Expected false from FilterSkipNth but received true")
	}

}

// Test FilterSkipNth successful
func TestFilterSkipNth_successful(t *testing.T) {
	f := filter.FilterSkipNth{N: 1}
	result := f.Filter(testProperty)

	if !result {
		t.Errorf("Expected true from FilterSkipNth but received false")
	}

}

// Test FilterSkipNth unsuccessful
func TestFilterSkipNth_unsuccessful(t *testing.T) {
	f := filter.FilterSkipNth{N: 2}
	result := f.Filter(testProperty)

	if result {
		t.Errorf("Expected false from FilterSkipNth but received true")
	}

}
