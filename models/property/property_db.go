package property

import (
	log "github.com/Sirupsen/logrus"
	"github.com/jmoiron/sqlx"
)

// insert the property into the database
// returns error or nil
func (p *Property) Insert(db *sqlx.DB) error {

	insertQuery := `INSERT INTO properties(property_id, street_address, town, valuateion_date, value) VALUES(?, ?, ?, ?, ?)`
	_, err := db.Exec(insertQuery, p)
	if err != nil {
		log.Error("Property.Insert: ", err)
		return err
	}

	return nil
}
