package property

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

// custom type that represents Property entity
type Property struct {
	Id            int       `db:"property_id"`
	StreetAddress string    `db:"street_address"`
	Town          string    `db:"town"`
	ValuationDate time.Time `db:"valuateion_date"`
	Value         int       `db:"value"`
}

const ValuationDateLayout = "1/02/06"

// validate and compose new Property object from given string values
func NewFromRow(id, street, town, valuationDate, value string) (*Property, error) {
	p := new(Property)

	if err := p.SetId(id); err != nil {
		return nil, err
	}

	if err := p.SetStreetAddress(street); err != nil {
		return nil, err
	}

	if err := p.SetTown(town); err != nil {
		return nil, err
	}

	if err := p.SetValuationDate(valuationDate); err != nil {
		return nil, err
	}

	if err := p.SetValue(value); err != nil {
		return nil, err
	}

	return p, nil
}

// validate and set parsed ID
func (p *Property) SetId(id string) error {
	id = strings.TrimSpace(id)

	_id, err := strconv.Atoi(id)
	if err != nil {
		return err
	}
	p.Id = _id
	return nil
}

// validate and set parsed StreetAddress
func (p *Property) SetStreetAddress(street string) error {
	street = strings.TrimSpace(street)
	if len(street) == 0 {
		return errors.New("empty value for street")
	}

	p.StreetAddress = street
	return nil
}

// validate and set parsed Town
func (p *Property) SetTown(town string) error {
	town = strings.TrimSpace(town)
	if len(town) == 0 {
		return errors.New("empty value for town")
	}

	p.Town = town
	return nil
}

// validate and set parsed ValuationDate
func (p *Property) SetValuationDate(valuationDate string) error {
	parsedDate, err := time.Parse(ValuationDateLayout, valuationDate)
	if err != nil {
		return err
	}
	p.ValuationDate = parsedDate
	return nil
}

// validate and set parsed Value
func (p *Property) SetValue(value string) error {
	value = strings.TrimRight(value, "\r")

	_value, err := strconv.Atoi(value)
	if err != nil {
		return err
	}
	p.Value = _value
	return nil
}

// format the object into print friendly string
func (p *Property) String() string {
	return fmt.Sprintf("%d-%s-%s-%s-%d", p.Id, p.StreetAddress, p.Town, p.ValuationDate.Format(ValuationDateLayout), p.Value)
}

// format string to be used as key
func (p *Property) GetKey() string {
	return fmt.Sprintf("%d-%s", p.Id, p.ValuationDate.Format(ValuationDateLayout))
}
