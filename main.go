package main

import (
	"bitbucket.org/omie/homes/lib/filter"
	"bitbucket.org/omie/homes/lib/processor"

	log "github.com/Sirupsen/logrus"
)

func main() {
	//log.SetLevel(log.DebugLevel)
	log.SetLevel(log.InfoLevel)

	log.Info("Hello Homes!")

	filename := "properties.txt"

	// test1 - keep last record
	log.Info("--- Starting to process with LastRecord processor")
	lastRecord := new(processor.ProcessorLastRecord)
	processor.DoWork(filename, lastRecord, 1) // <-- NOTE: should not use 1+ goroutines as order is important

	// test2 - keep first record
	log.Info("--- Starting to process with FirstRecord processor")
	firstRecord := new(processor.ProcessorFirstRecord)
	processor.DoWork(filename, firstRecord, 1) // <-- NOTE: should not use 1+ goroutines as order is important

	// test3 - no record if found duplicates
	log.Info("--- Starting to process with NoDuplicates processor")
	noDup := new(processor.ProcessorNoDuplicateRecord)
	processor.DoWork(filename, noDup, 1)

	// test4 - run filters over the deDup set
	// + extra task of running it in go routines
	log.Info("--- Starting to process with NoDuplicates+Filters processor")
	noDupFilter := new(processor.ProcessorNoDuplicateRecord)
	// setup some filters
	filters := make([]filter.Filter, 0)
	filters = append(filters, &filter.FilterValueMin{MinValue: 400000})
	filters = append(filters, &filter.FilterStreetAddressIgnoreString{FilterString: "AVE"})
	filters = append(filters, &filter.FilterStreetAddressIgnoreString{FilterString: "CRES"})
	filters = append(filters, &filter.FilterStreetAddressIgnoreString{FilterString: "PL"})
	filters = append(filters, &filter.FilterSkipNth{N: 10})

	noDupFilter.SetFilters(filters...)
	processor.DoWork(filename, noDupFilter, 4) // <-- NOTE: running it in 4 goroutines

	log.Info("Bye Homes!")
}
